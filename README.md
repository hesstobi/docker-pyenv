# pyenv

This is a base image for installing multiple Python versions with [pyenv](https://github.com/yyuu/pyenv).

To create your one image create a docker file wiht this content:

```
FROM registry.energiekoppler.com/ek/tools/docker-pyenv
```

and add a python-versions.txt where you put in the pyenv python version which are schould be installed on the image.

```
3.7.5
3.8.0
```
